var express = require ("express");
var path = require ("path");
var mongoose = require ("mongoose");
var bodyParser = require('body-parser');
var logger = require('morgan');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
mongoose.Promise = require("bluebird");

mongoose.connect(process.env.MONGO_URI, function(err){
    if (err) {throw err;}
    console.info("Connection to the database was successfull");
});


var app = express();

//use middlewares
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser());

//static files
app.use(express.static(path.join(__dirname, "..", "client")));


//public api
app.use("/api/calendar", require("./calendar"));

//serve always index.html
app.get('/*', function(req, res){
    res.sendFile(path.join(__dirname, "..", "client", "index.html"));
});

//server
app.listen(8080, function(){
    console.log ("Server Run");
})