var mongoose = require ("mongoose");
var Schema = mongoose.Schema;
var appointmentSchema = new Schema({
   title: String,
   description: String,
   deadline: Date,
   priority: String,
   place: String,
   buttonEnable: {
       type: String,
       default: 'disabled'
   },
   created:{
       type: Date,
       default: Date.now
   },
   done:{
       type: String,
       default: "noDone"
   }
});

var Appointment = mongoose.model("Appointment", appointmentSchema);
module.exports = Appointment;