var express = require("express");
var controller = require ("./calendar.controller.js")();
var router = express.Router();


router.delete('/:id', controller.remove);
router.post('/', controller.add);
router.get('/', controller.getAppointment);
module.exports = router;