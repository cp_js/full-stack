var Appointment = require("./calendar.model.js");
var mongoose = require("mongoose");
module.exports = function(){

    var add = function(req, res){
        var id = req.body._id || mongoose.Types.ObjectId();
        
        Appointment.findByIdAndUpdate(id, req.body, {upsert: true}).exec().then( function(){
            console.log("then");
            res.status(200).send("CalendarController: Success appoinement saved");
        }).catch(function (err){  
            console.log("catch");
            res.status(500).send("CalendarController: Save Error appointment");
        });
    };                                  
    
    var getAppointment = function(req, res){
        Appointment.find().exec().then( function(appointment){
            res.json(appointment);
        }).catch( function (err){
            res.status(500).send("CalendarController: getAppointment Error");
        });
    };
   
    var remove = function (req, res){
        Appointment.findByIdAndRemove(req.params.id).exec().then(function (){
            res.status(200).send("Appointment remove");
        }).catch( function(err){
            res.status(500).send("ControllerCalendar: fail delete appointments")
        })
    }
    

    return {
        add: add,
        getAppointment: getAppointment,
        remove: remove,
    }
}