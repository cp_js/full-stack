angular.module('Calendar', [
    'ngResource','ngMaterial','ui.router', 'lr.upload'
])

  .config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('blue-grey')
      .accentPalette('cyan')
      .warnPalette('red')
      .backgroundPalette('grey');
  })

