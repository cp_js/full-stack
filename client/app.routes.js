angular.module('Calendar')
        .config(function ($stateProvider, $locationProvider, $urlRouterProvider) {
            
            $locationProvider.html5Mode(true).hashPrefix('!');

            $urlRouterProvider.otherwise('/calendar/home');
            
            $stateProvider

                .state ('home' ,  {
                    url: '/calendar/home',
                    templateUrl: 'appointment/home/home.template.html',
                    controller: 'CalendarController',
                })
                
                .state ('appointment', {
                    url: '/calendar/appointment',
                    templateUrl: 'appointment/form/form.template.html',
                    controller: 'CalendarController'
                })
                
                .state ('search', {
                    url: '/calendar/search',
                    templateUrl: '/appointment/search/search.template.html',
                    controller: 'CalendarController'
                })
                
                
        });