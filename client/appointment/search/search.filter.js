angular.module('Calendar').filter('filterByDate', function () {

    return function (input, deadline){
        if(deadline === undefined){ return input };
        var array = [];
        if (deadline){
            array = deadline.split("/");
            if (array[0]){
                var x = parseInt(array[0]) -1;
                array[0] = x.toString();
            };
            array.reverse();
            deadline = array.toString().replace(/,/g,"-") + "T23:00:00.000Z";
        };

            return input.filter(function (input){
               var appointment = {}
                for (appointment in input){
                    if (appointment.deadline != deadline){
                        delete appointment;
                    }
                }
                return input;
            });
            
    };
  
});

