angular.module("Calendar").directive('appointments', function (){
   
   return{
       restrict: "E",
       controller: "CalendarController",
       templateUrl: "./appointment/customDirective/appointments.template.html"
   } 
});