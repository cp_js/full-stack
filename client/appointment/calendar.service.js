angular.module('Calendar').factory('CalendarService', function($resource){
    
    
    var Appointment = $resource('/api/calendar/:id/:action', {
        id: '@id',
        action: '@action',
    })
    
    
    var save = function(appointment, callback){
        callback = callback || angular.noop;
        return Appointment.save(appointment, function(){
            return callback();
        },function(err){
            console.log("CalendarService: Error");
            return callback(err);
        }).$promise;
    };
   
    
    var query = function( callback){
        callback = callback || angular.noop;
        return Appointment.query( function(appointments){
            return callback(appointments);
        }, function (err){
            console.log("CalendarController Error: query");
            return callback(err);
        }).$promise;
    };
    
     
    var remove = function( params, callback){
        callback = callback || angular.noop;
        return Appointment.remove(params, function(){
            return callback();
        }, function (err){
            console.log("CalendarService Error: fail remove appointment");
            return callback(err);     
        }).$promise;
    };
    
        
    return {
        save: save,
        query: query,
        remove: remove,
    }
});






