angular.module('Calendar').controller('CalendarController', function ($scope, $state, CalendarService){
   
   $scope.CalendarService = CalendarService;
   $scope.appoinment = {
       title: "",
       description: "",
       priority: "",
       deadline: "",
       done: "",
       place: ""
   };
    
    
   $scope.priorities = ('red yellow green').split(' ').map(function(priority){
       return {code: priority};
   });
   
   
   $scope.save = function(appoinment){
       CalendarService.save(appoinment).then(function(){
           $state.go('home');
       }).catch( function(err) {
           console.log("CalendarController: Error send appointment to CalendarService");
       }).$promise;
   };
    
    
    $scope.remove = function(appointments){
        CalendarService.remove({id: appointments._id}, function(){
            loadAppointment();
        }, function (err){
            console.log("CalendarController, remove failed");
        });
    };
    

    $scope.stausAppointment = function(appointment){
        appointment.done = appointment.done== 'noDone' ? 'done': 'noDone';
        appointment.buttonEnable = appointment.done== 'noDone' ? 'disabled': null;
        CalendarService.save(appointment);
    };
   
   
   var loadAppointment = function(callback){
        callback = callback || angular.noop;
        CalendarService.query().then( function(appointments){
            $scope.appointments = appointments;
        }).catch( function(err){
            console.log("CalendarController: err loadAppointemet()"); 
        });
    }


    
    loadAppointment();

});



